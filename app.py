import os
from flask import Flask, render_template
from flask.ext import assets

app = Flask(__name__)
env = assets.Environment(app)

env.load_path = [
    os.path.join(os.path.dirname(__name__), 'sass'),
    os.path.join(os.path.dirname(__name__), 'coffee'),
    os.path.join(os.path.dirname(__name__), 'bower_components'),
]

env.register(
    'js_all',
    assets.Bundle(
        'foundation-apps/dist/js/foundation-apps-templates.min.js',
        'foundation-apps/dist/js/foundation-apps.min.js',
        'angular/angular.min.js.map',
        'angular/angular.min.js',
        assets.Bundle(
            'app.coffee',
            filters=['coffeescript'],
        ),
        output='js/js_all.js'
    )
)

env.register(
    'css_all',
    assets.Bundle(
        'foundation-apps/dist/css/foundation-apps.min.css',
        'all.sass',
        filters=['sass'],
        output='css/css_all.css'
    )
)

@app.route('/')
def hello():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
