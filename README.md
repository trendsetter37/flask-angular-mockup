# Flask Angular Mockup
---
### Global Dependencies
* Bower
* npm
* python
* pip
* Ruby or gem
* sass
* gulp

Install global & app dependencies.
```
npm install -g coffee-script gulp bower
bower install
pip install -r requirements.txt
gem install -g sass
```
